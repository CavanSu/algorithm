//
//  main.c
//  Selected
//
//  Created by CavanSu on 2020/5/6.
//  Copyright © 2020 CavanSu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char * argv[]) {
    int arrayCount = 7;
    int array[arrayCount];
    srand((int)time(0));
    
    // int array[arrayCount] = {5, 3, 6, 2, 8, 9 , 0 , 1, 4, 7};
    
    for (int i = 0; i < arrayCount; i++) {
        int result = rand() % 100;
        array[i] = result;
        printf("preview: %d\n", array[i]);
    }
    
    int min = 0, temp = 0;
    int count1 = 0, count2 = 0;
    
    // 每一次遍历，每相邻两个做对比，找出最小的数放在 k 的位置
    // 最差时间复杂度 O(n^2)
    
    for (int k = 0; k < arrayCount - 1; k ++) {
        
        min = k;
        
        for (int j = k + 1; j < arrayCount; j ++) {
            
            count1 ++;
            
            if (array[j] < array[min]) {
                min = j;
            }
        }
        
        //
        if (min != k) {
            count2 ++;
            
            temp = array[min];
            array[min] = array[k];
            array[k] = temp;
        }
    }
    
    printf("total traversal: %d, exchange: %d \n", count1, count2);
    
    for (int i = 0; i < arrayCount; i++) {
        printf("sorted: %d\n", array[i]);
    }
    
    return 0;
}
