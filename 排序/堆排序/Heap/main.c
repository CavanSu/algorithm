//
//  main.c
//  Heap
//
//  Created by CavanSu on 2020/5/26.
//  Copyright © 2020 CavanSu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// 大顶堆，双亲节点大于左右孩子节点，小顶堆相反

// 完全二叉树，双节点 i , 左孩子节点则为 2i, 右孩子节点则为 2i + 1
// 1 <= i <= [n / 2](向下取整)

// 堆排序就是利用堆进行排序的算法，它的基本思想是:
// 此时，整个序列的最大值就是堆顶的根节点，将它移走（就是将其与堆数组的末尾元素交互，此时末尾元素就是最大值）
// 然后将剩余的 n-1 个序列重新构造成一个堆，反复

void printArray(int array[], int arrayCount) {
    for (int i = 1; i <= arrayCount; i++) {
        printf("sorted: %d\n", array[i]);
    }
}

void swap(int k[], int i, int j) {
    int temp;
    temp = k[i];
    k[i] = k[j];
    k[j] = temp;
}


// s 双亲节点的下标
// arrayCount 是数组长度
// heapAdjust 每次排序每个双亲节点及它的子节点

void heapAdjust(int array[], int s, int arrayCount) {
    printf("parent index: %d", s);
    printf("\n");
    
    int i, temp;
    
    temp = array[s];
    
    // i = 2 * s, 表示 s 节点的左孩子
    for (i = 2 * s; i <= arrayCount; i *= 2) {
        
        // i 不是最后一个顶点，且左孩子小于右孩子
        if (i < arrayCount && array[i] < array[i + 1]) {
            // 让 i 指向最大值的孩子
            i ++;
        }
        
        // 如果双亲大于孩子，则退出循环
        if (temp >= array[i]) {
            break;
        }
        
        // 如果双亲小于孩子，将孩子的值放入双亲的位置
        array[s] = array[i];
        
        // 使 s 由原来的双亲下标，变为孩子的下标
        s = i;
        
        // 在下面的 k[s] = temp;
        // 将双亲的值放入孩子的位置
    }
    
    array[s] = temp;
    
    printArray(array, arrayCount);
    printf("\n");
}

// n 是数组长度
void heapSort(int array[], int arrayCount) {
    int i;
    
    // 从上往下， 从右往左遍历
    // 所以从 n / 2 的双亲节点的最小下标
    // 这个 for 循环能找出最大的根节点，也能使每个双亲节点大于子节点
    for (i = arrayCount / 2; i > 0; i --) {
        heapAdjust(array, i, arrayCount);
    }
    
    for (i = arrayCount; i > 1; i--) {
        // 将调整好的根节点与堆数组的最后一位互换
        swap(array, 1, i);
        
        heapAdjust(array, 1, i - 1);
    }
}

int main(int argc, const char * argv[]) {
    // 二叉树下标从 1 开始
    // array[0] 只是占个位子
    int array[10] = {-1, 5, 2, 6, 0, 3, 9 , 1 , 7, 4};
    int arrayCount = 9;
    // 大顶堆排序
    heapSort(array, arrayCount);
    
    printArray(array, arrayCount);
    return 0;
}
