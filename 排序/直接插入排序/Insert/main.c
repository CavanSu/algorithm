//
//  main.cpp
//  Insert
//
//  Created by CavanSu on 2020/5/20.
//  Copyright © 2020 CavanSu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char * argv[]) {

    int arrayCount = 7;
    int array[arrayCount];
    srand((int)time(0));
    
    // int array[arrayCount] = {5, 3, 6, 2, 8, 9 , 0 , 1, 4, 7};
    
    for (int i = 0; i < arrayCount; i++) {
        int result = rand() % 100;
        array[i] = result;
        printf("preview: %d\n", array[i]);
    }
    
    // temp 哨兵位，存放临时值
    int temp = 0;
    int i = 0;
    int j = 0;
    
    // 建立一个哨兵位，每次比较 前后两位(i - 1, i)，当 i 的值小于 (i - 1) 的值，就把 i 值付给哨兵位。
    // 然后从 （i - 1）的下标值开始往左与哨兵位比较，大于哨兵位的往右挪
    
    for (i = 1; i < arrayCount; i ++) {
        if (array[i] < array[i - 1]) {
            temp = array[i];
            
            for (j = i - 1; temp < array[j] & j >= 0; j --) {
                array[j + 1] = array[j];
            }
            
            array[j + 1] = temp;
        }
    }
    
    for (int i = 0; i < arrayCount; i++) {
        printf("sorted: %d\n", array[i]);
    }
    
    return 0;
}
