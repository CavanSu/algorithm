//
//  main.c
//  冒泡排序
//
//  Created by CavanSu on 2020/5/5.
//  Copyright © 2020 CavanSu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char * argv[]) {
    
    int arrayCount = 7;
    int array[arrayCount];
    srand((int)time(0));
    
    // int array[arrayCount] = {5, 3, 6, 2, 8, 9 , 0 , 1, 4, 7};
    
    for (int i = 0; i < arrayCount; i++) {
        int result = rand() % 100;
        array[i] = result;
        printf("preview: %d\n", array[i]);
    }
    
    int count1 = 0, count2 = 0;
    int flag = 0;
    
    // 每两个数进行比较，大的往后挪，一趟下来，最大的数在最后一位
    // 每趟结束后，最后一位一定是最大的，不再比较，然后比较剩下的几位。
    // 如果已经提前为正序，则不需要再比较，直接结束
    // 最差时间复杂度为 O(n^2)
    
    // 如果 flag 为 1，则代表整个数组已经都是正序
    for (int k = 0; k < arrayCount - 1 && flag == 0; k ++) {
        for (int j = 0; j < (arrayCount - k - 1); j ++) {
            flag = 1;
            count1 ++;
            
            int t0 = array[j];
            int t1 = array[j + 1];
            
            if (t0 > t1) {
                flag = 0;
                count2 ++;
                
                array[j] = t1;
                array[j + 1] = t0;
            }
        }
    }
    
    printf("total traversal: %d, exchange: %d \n", count1, count2);
    
    for (int i = 0; i < arrayCount; i++) {
        printf("sorted: %d\n", array[i]);
    }
    
    return 0;
}
