//
//  main.c
//  Hill
//
//  Created by CavanSu on 2020/5/21.
//  Copyright © 2020 CavanSu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char * argv[]) {
    int arrayCount = 7;
    int array[arrayCount];
    srand((int)time(0));
    
    // int array[arrayCount] = {5, 3, 6, 2, 8, 9 , 0 , 1, 4, 7};
    
    for (int i = 0; i < arrayCount; i++) {
        int result = rand() % 100;
        array[i] = result;
        printf("preview: %d\n", array[i]);
    }
    
    int temp = 0;
    int i = 0;
    int j = 0;
    int gap = arrayCount;
    
    // n - (n / 3 + 1) = (2 / 3)n - 1
    //
    
    // 分段进行排序，与直接插入排序思路类似，但是是以一定间距进行对比，而不是像直接插入排序一样，相邻两两做对比
    
    do {
        gap = gap / 3 + 1;
        
        for (i = gap; i < arrayCount; i ++) {
            
            if (array[i] < array[i - gap]) {
                temp = array[i];
                
                for (j = i - gap; array[j] > temp & j >= 0; j -= gap) {
                    array[j + gap] = array[j];
                }
                
                array[j + gap] = temp;
            }
        }
    } while(gap > 1);
    
    for (int i = 0; i < arrayCount; i++) {
        printf("sorted: %d\n", array[i]);
    }
    
    return 0;
}
